//Declaração de i para monitorar quantas vezes a função mostrarQuestao é ativada
var i=-1
//Declaração de j para percorrer os vetores durante o código
var j
//Declaração de um contador para registrar as respostas certas
var cont= 0

//Nessa variável estão todas as perguntas e respostas que vocês usarão no desafio
var perguntas = [
    {
        texto: 'O céu não é o limite',
        respostas: [
            { valor: 0, texto: 'Ter preguiça' },
            { valor: 1, texto: 'Quando der certo a gente faz' },
            { valor: 3, texto: 'Vamo pra cima familia' },
            { valor: 2, texto: 'Se der for viavel a gente faz' }
        ]
    },
    {
        texto: 'Apaixonados pela missão',
        respostas: [
            { valor: 0, texto: 'Projetos nada mais sao do que numeros' },
            { valor: 1, texto: 'Dinheiro é bom e é isto' },
            { valor: 3, texto: 'Se meu projeto nao ajudar meu cliente entao nao fiz nada' },
            { valor: 2, texto: 'Se eu entender o que esta no codigo entao ta show' }
        ]
    },
    {
        texto: 'Ohana',
        respostas: [
            { valor: 0, texto: 'Ta com problema? Se vira filhao' },
            { valor: 3, texto: 'Voce é meu irmao de batalha, to contigo e nao solto' },
            { valor: 2, texto: 'Voce é da gti, eu tambem entao tudo certo' },
            { valor: 1, texto: 'Fora da Gti nem olho' }
        ]
    },
    {
        texto: 'Integridade',
        respostas: [
            { valor: 0, texto: 'Furar a fila do ru' },
            { valor: 2, texto: 'farol amarelo pra mim é verde' },
            { valor: 1, texto: 'Clonar cartao de credito' },
            { valor: 3, texto: 'Sou integro' }
        ]
    },
    {
        texto: 'Ser capitão da nave',
        respostas: [
            { valor: 3, texto: 'Eu serei o lider que eu quero pra mim' },
            { valor: 0, texto: 'Eu nao sabia, entao nao fiz' },
            { valor: 1, texto: 'Ve la, depois me conta' },
            { valor: 2, texto: 'To so esperando meu estagio aprovar pra sair da GTi' }
        ]
    },
    {
        texto: 'Mas e essa gti aí é o que',
        respostas: [
            { valor: 3, texto: 'Uma família-empresa top' },
            { valor: 1, texto: 'Uma igreja mormom' },
            { valor: 2, texto: 'Uns universitarios brincando de ser empresário' },
            { valor: 0, texto: 'Uma marca de refrigerante' }
        ]
    }
]

//Função que faz uma nova questão aparecer
function mostrarQuestao() {
    //Condicional que só deixa a próxima questão aparecer se alguma entrada estiver preenchida
    if(document.getElementsByClassName("input")[0].checked || document.getElementsByClassName("input")[1].checked || document.getElementsByClassName("input")[2].checked || document.getElementsByClassName("input")[3].checked || i==-1){
        //i registra quantas vezes entrou na função
        i=i+1
        //Se i já tiver revistrado todas as questões, então executar função finalizarQuiz
        if(i>=6){
          //Antes de finalizar o quiz, contabilizar o último valor
            for(j=0;j<4;j++){
                if(document.getElementsByClassName("input")[j].checked){
                    //Se não for a primeira pergunta, checar a resposta anterior
                    if(i!=0){
                      //Cont soma o valor da pergunta marcada
                      cont+=perguntas[i-1].respostas[j].valor
                    }
                    
                }
            }
            finalizarQuiz()
           
        }else{
            //dentrto do meu html, os elementos que tiverem a tag input, vão ter o display block, ou seja, vão aparecer
            document.getElementById("listaRespostas").style.display = "block"
            //Toda vez que recomeçar o for (executar a função mostrarQuestao), conferir a resposta anterior
            for(j=0;j<4;j++){
                if(document.getElementsByClassName("input")[j].checked){
                    //se o texto do valor 3 (resposta certa) estiver checado, então o contador registra
                    if(i!=0){
                      cont+=perguntas[i-1].respostas[j].valor
                    }
                    
                }
            }
            for(j=0;j<4;j++){
                //Reseta todos os inputs caso estejam checados
                document.getElementsByClassName("input")[j].checked = false
            }
            for(j=0;j<4;j++){
                //O título recebe a pergunta e as alternativas recebem as novas alternatiivas certas
                document.getElementById("titulo").innerHTML = perguntas[i].texto
                document.getElementsByClassName("alt")[j].innerHTML = perguntas[i].respostas[j].texto
            }
            //O botão recebe o novo conteúdo
            document.getElementById("confirmar").innerHTML = "Próxima"
        }
    }

}

function finalizarQuiz() {
    //O botão e a lista de respostas são inativados, a pergunta recebe o título inicial e o resultado recebe a porcentagem de acerto
    document.getElementById("titulo").innerHTML = "QUIZ VALORES GTI"
    document.getElementById("resultado").innerHTML = "Você acertou "+parseInt(cont*100/18)+"%"
    document.getElementById("confirmar").style.display = "none"
    document.getElementById("listaRespostas").style.display = "none"
}
